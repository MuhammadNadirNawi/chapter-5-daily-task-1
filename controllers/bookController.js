const {pool} = require('../config/database')


// API buat users
const getUsers = (request, response) => {
  pool.query('SELECT * FROM users', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getUserById = (request, response) => {
  const id = request.params.id

  pool.query('SELECT * FROM users WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createUser = (request, response) => {
  const {name} = request.body

  pool.query('INSERT INTO users (name) VALUES ($1) RETURNING *', [name], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send(`User added with ID: ${results.rows[0].id}`)
  })
}

// const updateUser = (request, response) => {
//   const id = request.params.id
//   const {name} = request.body

//   pool.query(
//     'UPDATE users SET name = $1 WHERE id = $2',
//     [name, id],
//     (error, results) => {
//       if (error) {
//         throw error
//       }
//       response.status(200).send(`User modified with ID: ${id}`)
//     }
//   )
// }

const deleteUsers = (request, response) => {
  const id = request.params.id

  pool.query('DELETE FROM users WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`User deleted with ID: ${id}`)
  })
}

// API buat books

const getBooks = (request, response) => {
  pool.query('SELECT * FROM books ORDER BY id ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const getBookById = (request, response) => {
  const id = request.params.id

  pool.query('SELECT * FROM books WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createBook = (request, response) => {
  const {title_book, writer, year_publish, user_id} = request.body

  pool.query('INSERT INTO books (title_book, writer, year_publish, user_id) VALUES ($1, $2, $3, $4)  RETURNING *', [title_book, writer, year_publish, user_id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send(`User added with ID: ${results.rows[0].id}`)
  })
}

const updateBook = (request, response) => {
  const id = parseInt(request.params.id);
  const {title_book, writer, year_publish, user_id} = request.body

  pool.query(
    'UPDATE books SET title_book = $1, writer = $2, year_publish = $3, user_id = $4 WHERE id = $5',
    [title_book, writer, year_publish, user_id, id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`User modified with ID: ${id}`)
    }
  )
}

const deleteBook = (request, response) => {
  const id = request.params.id

  pool.query('DELETE FROM books WHERE id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`User deleted with ID: ${id}`)
  })
}

module.exports = {
  getUsers,
  getUserById,
  createUser,
  deleteUsers,
  getBooks,
  getBookById,
  createBook,
  updateBook,
  deleteBook
}