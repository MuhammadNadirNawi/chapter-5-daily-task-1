// const dotenv = require('dotenv')
// dotenv.config()

const express = require("express")
const bodyParser = require("body-parser")
const path = require("path")

// our own module
const routes = require("./routes")

const port = 8000

// Intializations
const app = express();

// Settings view engine
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");

// Set JSON
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
// app.use(express.json())

// Public
app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "controller")));

// Routes
app.use(routes)

//Server Run
app.listen(port, () => {
    console.log(`Server running on ${Date(Date.now)}`)
    console.log(`Server listening on PORT: ${port}`)
});
