// Add Data Product in dashboard admin
async function addDataProduct() {
  const data = {
    title_book: document.getElementById("addTitleBook").value,
    writer: document.getElementById("addWriter").value,
    year_publish: document.getElementById("addYearPublish").value,
    user_id: document.getElementById("addUserId").value,

  };
  if ( data.title_book == "" ) {
    alert("Data Tidak Boleh Kosong!");
  }
  else if ( data.writer == "") {
    alert("Data Tidak Boleh Kosong!");
  }
  else if ( data.year_publish == "") {
    alert("Data Tidak Boleh Kosong!");
  }
  else if ( data.user_id == "") {
    alert("Data Tidak Boleh Kosong!");
  }
   else {
    const restAPI = await axios.post("/api/books", data);
    if (restAPI.data.status) {
      alert("Berhasil Tambah Data");
    } else {
      alert("500 Server Error [Tidak Dapat Menambahkan data]");
    }
  }
}
