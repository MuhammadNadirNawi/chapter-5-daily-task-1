const router = require("express").Router();
const bookController = require('../controllers/bookController.js')
const adminbookController = require('../controllers/admin/bookController.js')

// API users
router.get('/api/users', bookController.getUsers)
router.get('/api/users/:id', bookController.getUserById)
router.post('/api/users', bookController.createUser)
router.delete('/api/users/:id', bookController.deleteUsers)


// API books
router.get('/api/books', bookController.getBooks)
router.get('/api/books/:id', bookController.getBookById)
router.put('/api/books/:id', bookController.updateBook)
router.post('/api/books', bookController.createBook)
router.delete('/api/books/:id', bookController.deleteBook)


router.get('/', adminbookController.getUserId)
// router.get('/', adminbookController.getBooks)
router.get('/addBook', adminbookController.createBook)
router.get('/viewBook/:id', adminbookController.viewBook)
router.get('/editBook/:id', adminbookController.editBookPage)
router.post('/updateBook/:id', adminbookController.editBook)
router.get("/hapus/:id", adminbookController.hapusBook);

// router.put('/editBook/:id', adminbookController.editBook)


module.exports = router
